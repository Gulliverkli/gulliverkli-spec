# gulliverkli-spec

[Specification](spec/contents.md)

---
### Wie kann man diese Spec bearbeiten?

Am einfachsten sollte es sein, sich die IntelliJ Community Edition
zu installieren. Ich verwende den Markdown editor, der immer auch gleich
eine Preview bietet.

### Ablauf
1. Project auf Codeberg clonen
2. Clone in Intellij auschecken / mermaid plugin für IntelliJ installieren und aktivieren
3. Branch für PR anlegen
4. Ändern
5. Änderungen committen und ggf. pushen
6. auf codeberg aus dem brach eine pull request machen
7. mit dem nächsten brach für die nächste Überarbeitung anfangen

