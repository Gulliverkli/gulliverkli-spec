# Table of Contents

1. [Overview](overview/overview.md)
1. [Model/Meta Model](model/model-metamodel.md)
3. [Spreadsheet aware models](spreadsheet-aware-model/spreadsheet-aware-model.md)
4. [Definitions & Rules](definitions-rules/definitions-rules.md)
5. [UI](ui/ui.md)
