[Table of Contents](../contents.md)

---
# Overview

Es gibt eine Reihe von Anwendungen, bei denen die Daten aus einem Modell mit einer 
begrenzen Menge an Entit�ten (< 20)
visualisiert werden sollen. F�r solche F�lle ist ein generisches Tool (z.B. Spotfire) 
zu abstrakt und Excel zu speziell.

Diese L�cke soll **Gulliverkli** f�llen.

### Das Ziel

Im Wesentlichen sollen 2 Ziel erreicht werden:
1. Die Struktur der Daten (also das Modell) soll in Gulliverkli weiter erhalten bleiben. Damit geht
keine Information verloren und die Daten k�nnen flexibel ausgewertet werden.
2. Durch den Verzicht auf ein generisches Modell soll die Verst�ndlichkeit der Daten und ihrer Zusammenh�nge
f�r den Anwender erhalten bleiben. Es wird nicht mit abstrakten Tabellen und Relationen gearbeitet - die
Daten bleiben in der Problem Domain.

### Die Idee
Um Daten in Gulliverkli zu verwenden, muss zus�tzlich zu den eigentlichen Daten
eine entsprechende Modellbeschreibung (das Meta Modell) vorliegen.
Aus dem Meta Modell kann automatisch ein intuitiv zu benutzendes UI abgeleitet werden, dass
* hinreichend konkret ist, damit sich der Anwender im Modell orientieren kann
* beliebige Visualisierungen durch den Anwender dynamisch erzeugt werden k�nnen

Die Pr�sentation der Daten basiert dabei immer auf einer tabellarischen Darstellung.
Im einfachsten Fall ist das z.B. ein Spreadsheet mit Zeilen/Spalten/Zellen.
Die m�glichen Transformationen der Modelldaten in eine solche tabellarische Form wird im Abschnitt
[Spreadsheet aware models](spreadsheet-aware-model/spreadsheet-aware-model.md) im Detail erkl�rt.

Diese tabellarische Darstellung eignet sich auch als Grundlage f�r Charts - dabei beschreibt z.B. eine
Spalte eine Linie im Chart. Weitere Spalten k�nnen dann f�r z.B. Tooltips f�r Punkte im Diagramm
verwendet werden.

Auch weitere Visualisierungen sind auf dieser Basis denkbar, so k�nnten die Daten einer Zeile auf
einem Formular dargestellt werden - wobei dann durch Bl�ttern zwischen der Darstellung der Zeilen gewechselt
werden kann (oder auch mehrere Formulare 'gekachelt' angezeigt werden k�nnen).

Den Inhalt der tabellarischen Darstellungen (also die darzustellenden Zeilen und Spalten) kann 
der Anwender frei w�hlen. Da mit konkreten Modellen (Metamodellen) gearbeitet wird, ist das Zusammenstellen einfach 
und intuitiv m�glich.

Es k�nnen gleichzeitig beliebig viele dieser tabellarischen Sichten auf die Daten definiert werden.
Sie spiegeln alle jeweils den aktuellen Stand der zugrundeliegenden Daten wider.

Um komplexere Sichten bzw. Auswertungen zu unterst�tzen k�nnen neben der einfachen Verwendung von
Modelldaten in den tabellarischen Sichten (als Spalten) auch Pivotierungen der Daten verwendet
werden. Ausserdem k�nnen auf dem Modell basierenden Funktionen definiert werden, die dann ebenfalls
als Spalten verwendet werden k�nnen.

Gulliverkli kann die Daten anderen Applikationen �ber diverse Schnittstellen zur Verf�gung stellen:
* die Modelldaten selbst - falls die Schnittstelle komplexe Datenstrukturen unterst�tzt (z.B. OData) 
* alle tabellarischen Sichten auf die Daten f�r Applikationen, die (nur) 'flache' Sichten 
unterst�tzen (z.B. Excel, Charting Tools)
