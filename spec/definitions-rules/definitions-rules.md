[Table of Contents](../contents.md)

---

# Definitionen

### FlatView
> Eine **FlatView** beschreibt ein internes Modell, das aus Zeilen,
Spalten und Zellen besteht. Jede Zelle gehört dabei eindeutig zu einer
Spalte und zu einer Zeile. Eine Zelle kann einen oder mehrere Werte
enthalten.

#### FlatView-Column
> Columns in einer FlatView
> * verweisen stets auf genau eine Property einer Entität
> * habe immer genau eine Aggregation, die stets für die Zusammenfassung der Werte pro Zelle genutzt wird.
> * TODO - Functions colums / Pivot Columns

## Perspektive
> Wenn im Modell mehr als eine Tabelle vorliegt, dann muss für die **FlatView** festgelegt werden,
welche Entität aus dem Modell die Grundlage der Daten für eine Zeile liefert. Diese Festlegung wird
als **Perspective** der FlatView bezeichnet.

### Aggregation

> Eine **Aggregation** ist eine Funktion, die eine Menge von Werten mit gleichem Datentyp
auf eine neue Menge von Werten abbildet. Dabei wird zwischen **Single Value Aggregationen** und
**Multi Value Aggregationen** unterschieden.

> Eine **Single Value Aggregation** bildet eine Menge von Werten mit gleichem Datentyp auf einen
einzelnen Wert (üblicherweise mit dem gleichen Datentyp) ab.

> Eine **Multi Value Aggregation** bildet eine Menge von Werten mit gleichem Datentyp auf eine
neue Menge ab.


# Regeln

> Jede FlatView hat stets genau eine Perspektive. Die Perspektive kann jederzeit geändert werden,
die Darstellung der FlatView passt sich entsprechend an. Die für die Änderung zur Auswahl stehenden
Perspektiven hängen von Metamodell und den aktuell in der **FlatView** verwendeten 
Entitäten/Properties ab.

> Die Änderung der **Perspektive** auf eine neue Entität ist genau dann möglich, wenn für alle aktuell
in der **FlatView** verwendeten Properties gilt: Die zur Property gehörende Entität ist von der Entität
> der Zielperspektive im Metamodell über genau einen Weg erreichbar.

Todo: das würde eine Assay Perspektive erlauben - eventuell muss man einige Übergänge im Metamodell verbieten
Ansatz: Man kann im Metamodell verschiedene Layer definieren (z.B. Assay); Entitäten im gleichen Layer sind
dann als Perspektive aus der aktuellen Perspektive heraus verboten.

> Eine Property einer Entität kann einer **FlatView** nur dann hinzugefügt werden, 
wenn die zur Property gehörende Entität von der aktuellen Perspektive der FlatView aus
im Metamodell über genau einen Weg erreichbar ist.


> Eine Property einer Entität kann einer **FlatView** beliebig oft hinzugefügt werden. Die so
entstehenden Columns können unterschiedliche Aggregationen haben.



