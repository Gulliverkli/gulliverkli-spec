[Table of Contents](../contents.md)

---
# Complete Sample

Server

```mermaid
erDiagram
    Server ||--|{ VM : hosts
    Server {
        name string 
        ip string
    }

    VM {
        name string 
        ip string 
        operating_system string
    }    

    Person ||--|{ VM : owns
    Person {
        name string 
    }    

```