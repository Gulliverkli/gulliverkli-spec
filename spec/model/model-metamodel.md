[Table of Contents](../contents.md)

---
# Datenhaltung

In Gulliverkli wird streng zwischen den Daten und ihrer Repräsentation unterschieden.
Die Daten selbst werden im Modell abgelegt. Die Struktur dieses Modells wird durch
das Meta Modell (maschinenlesbar) beschrieben.

# Das Model
Die grundlegenden Elemente des Gulliverkli für die Datenhaltung sind **Data Tables**.
Der Aufbau und die Verknüpfungen der **Data Tables** werden im Meta Modell definiert. 

Eine **Data Table** ist eine Matrix und entspricht in weitestgehend einer 
Tabelle in einer relationalen DB.
Eine **Data Table** ist eine Matrix mit 0:n Spalten und (prinzipiell) beliebig vielen Zeilen (0:m).

Eine **Data Table** hat
* einen innerhalb des Gulliverklis eindeutigen Namen
* 0:n **Data Columns** mit jeweils
  * innerhalb der **Data Table** eindeutiger Name
  * **Data Column Type** (String, Int, Real, Timestamp, ... [TODO: Multi Value])  
* 0:m **Data Rows**
  * **Data Row ID** eine innerhalb der **Data Table** eindeutige Bezeichnung der **Data Row**
  * n **Data Values**, die jeweils eindeutig einer Column zugeordnet sind und die
jeweils den passenden Datentyp haben.

### Beispiel

Eine **Data Table** zur Speicherung von Informationen zu Servern könnte folgendermaßen aussehen.

| RowID | name    | ip      | os         | priceInEuro |
|-------|---------|---------|------------|-------------|
| 1     | emu     | 1.2.3.4 | Linux      | 1234.56     |
| 2     | penguin | 1.2.3.5 | Windows XP | 456.78      |
| 3     | eagle   | 1.2.3.6 | Windows 10 | 2345.67     |

**Data Table**
* Name: *Server*
* Data Columns
  * Column Name: *name*; Column Type: string 
  * Column Name: *ip*; Column Type: string
  * Column Name: *os*; Column Type: string
  * Column Name: *priceInEuro*; Column Type: double
* 3 **Data Rows**

  | emu     | 1.2.3.4 | Linux      |     1234.56 |
  |---------|---------|------------|-------------|

  | 2     | penguin | 1.2.3.5 | Windows XP | 456.78      |
  |-------|---------|---------|------------|-------------|
  
  | 3     | eagle   | 1.2.3.6 | Windows 10 | 2345.67     |
  |-------|---------|---------|------------|-------------|

# Metamodel

Das Metamodel enthält
* die Beschreibungen aller **Data Tables**
  * den namen
  * alle **Data Columns**
    * den namen
    * den typ
    * multi value flag
* alle Beziehungen zwischen den **Data Tables** (gerichtet)
  * start Entität
  * ende Entität

Alle Beziehungen zwischen den Entitäten sind 'komplett' - das heißt es ist immer eine
1:0-n Beziehung 0,0:n wird nicht unterstützt.
[TODO] Die 0 Rechts bedeutet, das ggf. keine Rückverknüpfung existiert - wollen wir das?

# Data Source
Alle **Data Tables** eines Gulliverkli's haben eine gemeinsame **Data Source**.
Die **Data Source** liefert initial die Daten für die verschiedenen **Data Tables**.
Die Daten der **Data Tables** können auf Basis der **Data Source** ggf. auch
aktualisiert werden.

Zusätzlich ist auch über die tabellarischen Ansichten eine Änderung der Daten möglich.
Dabei sind allerdings einige Regeln zu beachten, damit die Integrität der Daten erhalten
bleibt. Prinzipiell ist Gulliverkli nicht als Editor für die Daten gedacht, sondern hat
den Schwerpunkt klar bei den verschiedenen Darstellungen der Daten-

Auch andere Gulliverklis können ggf. als **Data Source** zum Einsatz kommen (merge).

Die **Data Source** eines Gulliverklis kann nicht verändert werden.

