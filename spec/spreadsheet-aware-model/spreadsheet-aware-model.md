[Table of Contents](../contents.md)

---
# Spreadsheet aware model; FlatView, Perspektive, Aggregation

Alle Visualisierungen der Daten in Gulliverki basieren auf einer Abbildung
der Modelle in eine entsprechende tabellarische Form 
die dann für Spreadsheets, Charts, etc. verwendet wird.
Diese tabellarische Form wird im Folgenden als **FlatView** bezeichnet.
Eine **FlatView** kann dabei aus den Daten von mehr als einer **Data Table** aufgebaut sein,
dazu werden dann wie unten beschrieben, die Informationen über die **Data Relations** genutzt.

### FlatView
> Eine **FlatView** beschreibt ein internes Modell, das aus Zeilen, 
Spalten und Zellen besteht. Jede Zelle gehört dabei eindeutig zu einer
Spalte und zu einer Zeile. Eine Zelle kann einen oder mehrere Werte
enthalten.

> **FlatViews** sind die einzige Möglichkeit, die Modell Daten (also die DataTables) zu visualisieren.


Die **FlatView** enthält alle Informationen, um die Werte für alle Zellen
eindeutig aus dem zugrundeliegenden Modell zu bestimmen.

Weiterhin gibt es eine fest definierte Menge an die **FlatView** ändernden Operationen, die basierend auf
dem Meta-Modell und dem aktuellen Zustand der **FlatView** erlaubt sind.

Beispiel für solche Operationen sind
* Hinzufügen/Entfernen von Spalten
* Ändern der Eigenschaften von Spalten (Aggregation, Formatierung, Position)
* Ändern der Perspektive (s.U.)
* ...

Die Spalten einer **FlatView** entsprechen nicht 1:1 den **Data Columns**,
sie bauen vielmehr auf diesen
auf und enthalten noch zusätzliche Informationen wie z.B.
* einen Namen (Label)
* die aktuell eingestellte Aggregation (s.u.)

Columns können auch auf Funktionen oder Pivots basieren.

#### Beispiel 1 (Modell mit nur einer Tabelle)

DataModel

```mermaid
erDiagram
    Server {
        name string 
        ip string
        os string
        priceInEuro double
        contact string
    }
```

Dieses Modell kann z.B. als FlatView in folgenden Weise dargestellt werden

| Name    | IP      | OS         |
|---------|---------|------------|
| emu     | 1.2.3.4 | Linux      |
| penguin | 1.2.3.5 | Windows XP |

Zum Erzeugen der **FlatView** muss der Anwender die verschiedenen Spalten auswählen 
und der FlatView hinzufügen. Dazu wird das Metamodell in der **Palette** zur Verfügung gestellt.

### Palette
> Die **Palette** ist eine UI Komponente, die alle Properties aller Entitäten aus dem Metamodell bereitstellt.
Die Properties werden dann verwendet, um den FlatView's entsprechende Spalten hinzuzufügen.

#### Beispiel 1 (Modell mit nur einer Tabelle)

Modell

```mermaid
erDiagram
    Server {
        name string 
        ip string
        os string
        priceInEuro double
        contact string
    }
```

Für die FlatView wurden in diesem Beispiel nur die Spalten Name und OS verwendet.

| Name    | OS         |
|---------|------------|
| emu     | Linux      |
| penguin | Windows XP |
| eagle   | Windows 10 |

Vom Anwender wurde die FlatView erweitert: die Spalte IP wurde hinzugefügt.

| Name    | OS         | IP      |
|---------|------------|---------|
| emu     | Linux      | 1.2.3.4 |
| penguin | Windows XP | 1.2.3.5 |
| eagle   | Windows 10 | 1.2.3.6 |


## Perspektive
> Wenn im Modell mehr als eine Tabelle vorliegt, dann muss für die **FlatView** festgelegt werden,
welche Entität aus dem Modell die Grundlage der Daten für eine Zeile liefert. Diese Festlegung wird
als **Perspective** der FlatView bezeichnet.

Die **Perspektive** einer **FlatView** kann geändert werden (auf die Bedingungen 
wird später eingegangen).

Beispiel (verschiedene **FlatViews** in Server Perspektive)

```mermaid
erDiagram
    Person ||--|{ Server : operates

    Server {
        name string 
        ip string
        os string
        priceInEuro double
        operator string FK
    }

    Person {
        zuname string 
        vorname string 
    }    
```

Für diese **FlatView* wurden die Perspektive _Server_ verwendet.

###### Perspective: Server

| Name    | OS         |
|---------|------------|
| emu     | Linux      |
| penguin | Windows XP |

In dieser Perspektive kann **auch** der zuname des operators angezeigt werden. Dafür muss vom Anwender
einfach die Property Name der Entität Person aus der Palette der **FlatView* hinzugefügt werden.

###### Persektive: Server

| Name    | OS         | Zuname  |
|---------|------------|---------|
| emu     | Linux      | Meier   |
| penguin | Windows XP | Schmidt |
| eagle   | Windows 10 | Schmidt |

Im Beispiel existiert zu jedem Server eindeutig eine Person - der Operator -. Damit kann bei der Darstellung
in der **FlatView* zu jeder Zeile (Perspektive = Server) eindeutig der Name der Person ermittelt werden.

Da mehrere Server von Fr. Schmidt betreut werden, kommt ihr Zunahme entsprechend mehrfach vor.


> Enthält die FlatView Properties anderer Entitäten (also nicht von der Perspektive), so werden die Werte 
für die entsprechenden Zeilen über die Beziehungen im Modell ermittelt. Abhängig von der Kardinalität
können dabei mehr als ein Wert für die Zelle ermittelt werden.  

Beispiel (**FlatView** in Person Perspektive)

```mermaid
erDiagram
    Person ||--|{ Server : operates

    Server {
        name string 
        ip string
        os string
        priceInEuro double
        operator string FK
    }

    Person {
        zuname string 
        vorname string 
    }    
```

Für diese **FlatView* wurden die Perspektive _Person_ verwendet.

###### Persektive: Person

| Zuname  | Vorname | Name                | OS                          |
|---------|---------|---------------------|-----------------------------|
| Meier   | Klaus   | emu                 | Linux                       |
| Schmidt | Sigrid  | eagle <br />pinguin | Windows 10 <br />Windows XP |

Da mehrere Server von Fr. Schmidt betreut werden, werden für sie jeweils mehrere Server aufgelistet.


### Aggregation

Wie im vorangegangen Beispiel zu sehen, können **FlatViews** multiple Werte in Zellen enthalten.
Dies ist oft unpraktisch (z.B. bei numerischen Zellen die für Charts verwendet werden sollen).
Deshalb wird für jede Spalte eine **Aggregation** definiert.

> Eine **Aggregation** ist eine Funktion, die eine Menge von Werten mit gleichem Datentyp
auf eine neue Menge von Werten abbildet. Dabei wird zwischen **Single Value Aggregationen** und
**Multi Value Aggregationen** unterschieden.

> Eine **Single Value Aggregation** bildet eine Menge von Werten mit gleichem Datentyp auf einen
einzelnen Wert (üblicherweise mit dem gleichen Datentyp) ab.

Beispiele: 
* Maximum
* Minimum
* Durchschnitt

> Eine **Multi Value Aggregation** bildet eine Menge von Werten mit gleichem Datentyp auf eine
neue Menge ab.

Beispiele:
* All
* All distinct

Beispiel (**FlatView** in Person Perspektive)

###### Perspektive: Person

| Zuname  | Vorname | Name (All)          | OS (All)                    | Price € (max) | Price € (min) |
|---------|---------|---------------------|-----------------------------|--------------:|--------------:|
| Meier   | Klaus   | emu                 | Linux                       |       1234.56 |       1234.56 |
| Schmidt | Sigrid  | eagle <br />pinguin | Windows 10 <br />Windows XP |       2345.67 |        456.78 |
